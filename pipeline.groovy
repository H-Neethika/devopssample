pipeline {
    agent any
    
    stages {
        stage('Dockerize the application') {
            steps {
                sh 'docker build -t sample .'
            }
        }
        stage('Verify Docker Images') {
            steps {
                sh 'docker images'
            }
        }
       stage('Run the docker container') {
            steps {
                sh 'docker run --name flask_app -p 8000:5000 sample'
            }
        } 
    }
}
